package Assignment7;

class SingletonObject {
    private int a;
    private int b;
    static SingletonObject singletonStatic;
    private SingletonObject(){}
    public void set(int a, int b){
        this.a = a;
        this.b = b;
    }
    public void get(){
        System.out.println("A "+a);
        System.out.println("B "+b);
    }
    static SingletonObject getObject(){
        if(singletonStatic == null){
            singletonStatic = new SingletonObject();
            System.out.println("Singleton Class Object created");
        }
        else
            System.out.println("Singleton Class Object not created just returned created one");
        return singletonStatic;
    }

    void display(String x){
        System.out.println(x);
    }

}

public class SingletonDemo {
    public static void main(String[] args) {
        SingletonObject aStatic = SingletonObject.getObject();
        SingletonObject aStatic1 = SingletonObject.getObject();
        aStatic.set(15, 20);
        aStatic1.get();
    }
}
