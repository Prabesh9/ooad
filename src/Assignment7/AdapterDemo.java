package Assignment7;

class Assignment{
    Pen p;
    public Assignment(Pen p){
        this.p = p;
    }
    public void write(String x){
        p.write(x);
    }
}
interface Pen{    void write(String x); }

class GelPen{
    public void draw(String x) {
        System.out.println(x + " from gel pen");
    }
}

class PenAdapter implements Pen{
    @Override
    public void write(String x) {
        new GelPen().draw(x);
    }
}

public class AdapterDemo {
    public static void main(String[] args) {
        Assignment assignment = new Assignment(new PenAdapter());
        assignment.write("Hello World");
    }
}
