package OOPClass;

import java.util.ArrayList;

public class Person {
    private int id;
    private String name;
    private ArrayList<Mobile> mobiles = new ArrayList<Mobile>(2);

    void Set(int id, String name) {
        this.id = id;
        this.name = name;
    }

    void display() {
        System.out.println("ID "+id);
        System.out.println("Name "+name);
        System.out.println("Displaying mobile details");
        System.out.println(".........................");

        mobiles.forEach(mobile -> {
            mobile.display();
        });
    }

    void ownsMobile(String mobileNumber, String brand) {
        if (mobiles.size() < 2) {
            Mobile newMobile = new Mobile();
            newMobile.set(mobileNumber, brand);
            newMobile.getsBill();
            mobiles.add(newMobile);
        }
    }

    public static void main(String[] args) {
        Person prabesh = new Person();
        prabesh.Set(16, "Prabesh");
        prabesh.ownsMobile("9818792669", "OnePlus");
        prabesh.ownsMobile("9813404590", "Huawei");
        prabesh.ownsMobile("9813162192", "Colors");
        prabesh.display();
    }
}


