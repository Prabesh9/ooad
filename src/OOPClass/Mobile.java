package OOPClass;

import java.util.Random;

public class Mobile {
    private String number;
    private String brand;
    private Bill bill = new Bill();

    void set(String number, String brand) {
        this.number = number;
        this.brand = brand;
    }

    void display() {
        System.out.println("    Mobile number "+number);
        System.out.println("    Brand "+brand);

        System.out.println("    Displaying bill details");
        System.out.println("    .........................");
        bill.display();
    }

    void getsBill() {
        Random random = new Random();
        int billNum = random.nextInt(100);
        bill.set(billNum, number, getAmount(number));
    }

    private long getAmount(String number) {
        return Long.parseLong(number)/400000;
    }
}
