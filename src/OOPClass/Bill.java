package OOPClass;

public class Bill {
    private int billNum;
    private String mobileNum;
    private double amount;

    void set(int billNum, String mobileNum, double amount) {
        this.billNum = billNum;
        this.mobileNum = mobileNum;
        this.amount = amount;
    }

    void display() {
        System.out.println("        Bill no. "+billNum);
        System.out.println("        Mobile no. "+mobileNum);
        System.out.println("        Amount Rs. "+amount);
    }
}
