package Assignment8;

interface Shape{    void display();     }

class Circle implements Shape{
    public void display(){
        System.out.println("This is circle.");
    }
}

class Rectangle implements Shape{
    public void  display(){
        System.out.println("This is rectangle.");
    }
}

class ShapeFactory{
    public Shape getShape(String str){
        switch (str){
            case "CIR":     return new Circle();
            case "REC":     return new Rectangle();
            default:        return null;
        }
    }
}

public class FactoryDemo {
    public static void main(String[] args) {
        ShapeFactory factory = new ShapeFactory();
        Shape cir = factory.getShape("CIR");
        cir.display();
    }
}
