package Demo;
import java.util.ArrayList;

class Channel{
    ArrayList<Subscriber> subscribers = new ArrayList<>();

    void addSubscriber(Subscriber subscriber){
        subscribers.add(subscriber);
    }

    void upload(String title){
        notify(title);
    }

    void notify(String title){
        subscribers.forEach(subscriber -> {
            subscriber.update(title);
        });
    }

}

abstract class SubscriberInterface{
    protected String name;
    protected Channel channel;
    abstract void update(String title);
}

class Subscriber extends SubscriberInterface{
    Subscriber(String name, Channel channel){
        this.name = name;
        this.channel = channel;
        channel.addSubscriber(this);
    }
    void update(String title){
        System.out.println(title+" video uploaded in "+name+"'s feeds.");
    }
}


public class Youtube {

    public static void main(String[] args) {
        Channel channel = new Channel();
        new Subscriber("Prabesh", channel);

        channel.upload("OOAD_EXAM");

        new Subscriber("Pranav", channel);

        channel.upload("OOAD_EXAM2");
    }

}
