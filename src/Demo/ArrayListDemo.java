package Demo;
import java.util.ArrayList;
class Animal{
    String name;
    Animal(String name){
        this.name = name;
    }
    void display(){
        System.out.println("Animal's name is "+name);
    }
}

class Dog extends Animal{
    String name;
    Dog(String name){
        super(name);
        this.name = name;
    }
    void display(){
        System.out.println("Dog's name is "+name);
    }
}

class Cat extends Animal{
    String name;
    Cat(String name){
        super(name);
        this.name = name;
    }
    void display() {
        System.out.println("Cat's name is "+name);
    }
}

public class ArrayListDemo {
    public static void main(String[] args) {
        ArrayList<Animal> AnimalList = new ArrayList<>();
        AnimalList.add(new Dog("Spike"));
        AnimalList.add(new Cat("Tom"));
        AnimalList.add(new Animal("Jerry"));
        for(Animal animal : AnimalList){
            animal.display();
        }
    }
}